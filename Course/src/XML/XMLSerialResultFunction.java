package XML;

import Model.FFunction;
import Model.GFunction;
import Model.Point;
import Model.ResultFunction;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRegistry;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

@XmlRegistry
public class XMLSerialResultFunction extends ResultFunction {

    private ResultFunction resultFunction;

    public ResultFunction getRF(){
        return resultFunction;
    }

    public void setResultFunction(ResultFunction F){
        resultFunction = F;
    }


    @Override
    public double getEnd(){
        return resultFunction.getEnd();
    }

    @Override
    public void setEnd(double end){
        resultFunction.setEnd(end);
    }

    @Override
    public double getStart(){
        return resultFunction.getStart();
    }
    @Override
    public void setStart(double start){
        resultFunction.setStart(start);
    }

    @Override
    public FFunction getfFunction() {
        return resultFunction.getfFunction();
    }

    @Override
    public void setfFunction(FFunction ff){
        resultFunction.setfFunction(ff);
    }

    @Override
    public GFunction getgFunction(){
        return resultFunction.getgFunction();
    }

    @Override
    public void setgFunction(GFunction gf){
        resultFunction.setgFunction(gf);
    }

    @Override
    public ArrayList<Point> getResultPointArrayList() {
        return resultFunction.getResultPointArrayList();
    }

    @Override
    public void setResultPointArrayList(ArrayList<Point> list){
        resultFunction.setResultPointArrayList(list);
    }

    public int xyCount() {
        return resultFunction.getResultPointArrayList().size();
    }

    @Override
    public double getX(int i) {
        return resultFunction.getResultPointArrayList().get(i).getX();
    }

    @Override
    public double getY(int i) {
        return getResultPointArrayList().get(i).getY();
    }

    @Override
    public void setX(int i, double x) {
        resultFunction.getResultPointArrayList().get(i).setX(x);
    }

    @Override
    public void setY(int i, double y) {
        getResultPointArrayList().get(i).setY(y);
    }

    @Override
    public double applyAsDouble(double operand) {
        return (resultFunction.getfFunction().y(operand) - resultFunction.getgFunction().y(operand));
    }

    public XMLSerialResultFunction writeResultFunction(String file) throws IOException, JAXBException {
        try  {
            JAXBContext jaxbContext = JAXBContext.newInstance(ResultFunction.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(resultFunction, new FileWriter(file));
            return this;
        }
        catch (IOException E){
            throw new IOException();
        }
    }

    public XMLSerialResultFunction readResultFuction(String fileName) throws IOException, JAXBException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ResultFunction.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            resultFunction = (ResultFunction) unmarshaller.unmarshal(new FileInputStream(fileName));
            return this;
        }
        catch (IOException  | JAXBException e) {
            e.printStackTrace();
            throw new IOException();
        }
    }
}
