package Controller;

import Model.*;
import Model.Point;
import XML.XMLSerialResultFunction;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;

import javax.xml.bind.JAXBException;
import java.awt.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    Button buttonStart;
    @FXML
    Pane chartPane;
    @FXML
    MenuItem clr;
    @FXML
    AnchorPane anchor;
    @FXML
    TextField txtFieldStart;
    @FXML
    TextField txtFieldEnd;
    @FXML
    TableView<Model.Point> tableView;
    @FXML
    TextField getTxtFieldResult;
    @FXML
    TextField functionF;

    NumberAxis xAxis;
    NumberAxis yAxis;

    LineChart<Number, Number> chtr;
    XYChart.Series series;

    FFunction fFunction = new FFunction();
    GFunction gFunction = new GFunction();
    ResultFunction resultFunction = new ResultFunction();

    double start, end, result;
    String ffunctionString;

    ObservableList<Point> observableList;
    @FXML
    private TableColumn<Point, Double> X;
    @FXML
    private TableColumn<Point, Double> Y;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tableView.setPlaceholder(new Label("Add point"));
        initChart();
        anchor.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(anchor.getHeight() <= anchor.getPrefHeight())
                    anchor.getScene().getWindow().setHeight(anchor.getPrefHeight()+40);
            }
        });
        anchor.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(anchor.getWidth() <= anchor.getPrefWidth())
                    anchor.getScene().getWindow().setWidth(anchor.getPrefWidth()+40);
            }
        });
        chartPane.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                resize();
            }
        });
        chartPane.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                resize();
            }
        });

    }

    public void initChart(){
        xAxis = new NumberAxis();
        yAxis = new NumberAxis();
        xAxis.setAutoRanging(false);
        xAxis.setLabel("X");
        yAxis.setLabel("Y");
        chtr = new LineChart<>(xAxis,yAxis);
        series = new XYChart.Series();
        series.setName("result function f(x) - g(x)");
        chtr.setCreateSymbols(false);
        chtr.setMinWidth(chartPane.getWidth());
        chtr.setMinHeight(chartPane.getHeight());
        chartPane.getChildren().add(chtr);
    }

    @FXML
    public void doAdd(ActionEvent event) {
        if (observableList == null || tableView.getItems() == null) {
            ArrayList<Point> list = new ArrayList<Point>();
            updateTable(list);
        }
        observableList.add(new Point(0, 0));
    }

    private void updateTable(ArrayList<Point> list) {
        observableList = FXCollections.observableList(list);
        tableView.setItems(observableList);

        X.setCellValueFactory(new PropertyValueFactory<>("X"));
        X.setCellFactory(
                TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        X.setOnEditCommit(t -> updateX(t));
        X.setSortable(false);
        Y.setCellValueFactory(new PropertyValueFactory<>("Y"));
        Y.setCellFactory(
                TextFieldTableCell.forTableColumn(new DoubleStringConverter()));
        Y.setOnEditCommit(t -> updateY(t));
        Y.setSortable(false);
    }

    @FXML
    public void doRemove(ActionEvent event) {
        if (observableList == null) {
            return;
        }
        if (observableList.size() > 0) {
            observableList.remove(observableList.size() - 1);
        }
        if (observableList.size() <= 0) {
            observableList = null;
        }
    }

    @FXML
    public void clear(ActionEvent event){
        resultFunction = new ResultFunction();
        functionF.setText("");
        txtFieldEnd.setText("");
        txtFieldStart.setText("");
        getTxtFieldResult.setText("");
        tableView.setItems(null);
        tableView.setPlaceholder(new Label("Add point"));
        observableList = null;
        series.getData().clear();
        chtr.getData().clear();
        chartPane.getChildren().clear();
        initChart();
    }

    @FXML
    public void doOpen(ActionEvent event) {
        FileChooser fileChooser = getFileChooser("Open XML-file");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);
        XMLSerialResultFunction xml = new XMLSerialResultFunction();
        File file;
        if ((file = fileChooser.showOpenDialog(null)) != null) {
            try {
                ResultFunction rf = xml.readResultFuction(file.getCanonicalPath());
                resultFunction = rf;
                txtFieldEnd.setText(String.valueOf(resultFunction.getEnd()));
                end = Double.parseDouble(txtFieldEnd.getText());
                txtFieldStart.setText(String.valueOf(resultFunction.getStart()));
                start = Double.parseDouble(txtFieldStart.getText());
                functionF.setText(resultFunction.getfFunction().getParseToFunction());
                ffunctionString = functionF.getText();
                tableView.setItems(null);
                updateTable(resultFunction.getgFunction().getPointListG());
            }
            catch (IOException e) {
                e.printStackTrace();
                showError("File not found");
            }
            catch (JAXBException e) {
                e.printStackTrace();
                showError("Incorrect file format");
            }
        }
    }

    @FXML public void doAbout(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("О программе...");
        alert.setHeaderText("Программа для нахождения максимума функции методом дихотомии. " + '\n' +
                "ПО создано в качестве курсового проекта студентом группы КН-35Г, Маликом И.Ю.");
        alert.setContentText("Версия 1.0");
        alert.showAndWait();
    }

    @FXML
    public void doSave(ActionEvent event) {
        FileChooser fileChooser = getFileChooser("Save XML-file");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);
        XMLSerialResultFunction xml = new XMLSerialResultFunction();
        xml.setResultFunction(resultFunction);
        File file;
        if ((file = fileChooser.showSaveDialog(null)) != null) {
            try {
                updateSourceData();
                xml.writeResultFunction(file.getCanonicalPath());
                showMessage("Successful saved");
            }
            catch (Exception e) {
                e.printStackTrace();
                showError("Error with saving!");
            }
        }
    }

    @FXML
    public void doExit(ActionEvent event) {
        Platform.exit();
    }

    private void showMessage(String s) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("");
        alert.setHeaderText(s);
        alert.showAndWait();
    }

    private void showError(String s) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(s);
        alert.showAndWait();
    }

    @FXML
    public void startChanged(ActionEvent event) {
        try {
            start = Double.parseDouble(txtFieldStart.getText());
        }
        catch (NumberFormatException e){
            start = 0;
        }
    }

    @FXML
    public void endChanged(ActionEvent event) {
        try {
            end = Double.parseDouble(txtFieldEnd.getText());
        }
        catch (NumberFormatException e){
            end = 0;
        }
    }

    @FXML
    public void ffunctionChanged(ActionEvent event) {
        ffunctionString = txtFieldStart.getText();
        resultFunction.getfFunction().setParseToFunction(ffunctionString);
    }

    private void updateSourceData() {
        gFunction.clearGList();
        for (Point c : observableList) {
            gFunction.addPoint(c);
        }
    }

    private void updateX(TableColumn.CellEditEvent<Point, Double> t) {
        Point c = t.getTableView().getItems().get(t.getTablePosition().getRow());
        c.setX(t.getNewValue());
    }

    private void updateY(TableColumn.CellEditEvent<Point, Double> t) {
        Point c = t.getTableView().getItems().get(t.getTablePosition().getRow());
        c.setY(t.getNewValue());
    }

    public static FileChooser getFileChooser(String title) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("XML-files (*.xml)", "*.xml"));
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("All files (*.*)", "*.*"));
        fileChooser.setTitle(title);
        return fileChooser;
    }

    double eps;
    @FXML
    public void execute(ActionEvent event){
        try{
            fFunction.setParseToFunction(functionF.getText());
            updateSourceData();
            for (Point p: gFunction.getPointListG()) {
                System.out.println(p);
            }
            resultFunction.setfFunction(fFunction);
            resultFunction.setgFunction(gFunction);
            resultFunction.setStart(start);
            resultFunction.setEnd(end);
            System.out.println(start);
            System.out.println(end);
            if(end - start > 1)
               eps = 1;
            else
                eps = 0.1;

            ArrayList<Point> ls = new ArrayList<>();
            for(double a = start; a <= end; a+=eps) {
                Point tp = new Point(a, resultFunction.y(a));
                System.out.println(tp);
                ls.add(tp);
            }
            resultFunction.setResultPointArrayList(ls);
            System.out.println();

            Dichotomy dichotomy = new Dichotomy(start, end, 0.00000000001, resultFunction);
            result = dichotomy.FindMax();
            result = new BigDecimal(result).setScale(3, RoundingMode.UP).doubleValue();
            System.out.println(result);
            getTxtFieldResult.setText(String.valueOf(result));
            drawGraphic();
        }
        catch (NumberFormatException e){
            showError("Incorrect input");
            e.printStackTrace();
        } catch (Exception e) {
            showError("Incorrect input");
            e.printStackTrace();
        }
    }

    public void resize(){
        chartPane.getChildren().clear();
        chtr.getData().clear();
        series.getData().clear();
        if (getTxtFieldResult.getText().equals("") || getTxtFieldResult.getText() == null) {
            xAxis = new NumberAxis();
            yAxis  = new NumberAxis();
            if(!((observableList == null) || (observableList.size() == 0))) {
                xAxis.setAutoRanging(false);
                xAxis.setLowerBound(start);
                xAxis.setUpperBound(end);
                yAxis.setAutoRanging(false);
                yAxis.setLowerBound(findMiny());
                yAxis.setUpperBound(result);
            }
            xAxis.setLabel("X");
            yAxis.setLabel("Y");
            chtr = new LineChart<>(xAxis, yAxis);
            chtr.setMinWidth(chartPane.getWidth());
            chtr.setMinHeight(chartPane.getHeight());
            chtr.setCreateSymbols(false);
            series = new XYChart.Series();
            series.setName("result function f(x) - g(x)");
            chartPane.getChildren().add(chtr);
        }
        else {
            buttonStart.fire();
        }
    }

    public double findMiny(){
        ArrayList<Double> resYlist = new ArrayList<>();
        for(double x = start; x <= end; x+=0.01){
            resYlist.add(resultFunction.y(x));
        }
        double minY = resYlist.get(0);
        for(int i = 1; i < resYlist.size(); i++){
            if(resYlist.get(i) < minY)
                minY = resYlist.get(i);
        }
        return minY;
    }

    public void drawGraphic(){
        chtr.setMinWidth(chartPane.getWidth());
        chtr.setMinHeight(chartPane.getHeight());
        chartPane.getChildren().clear();
        chtr.getData().clear();
        series.getData().clear();
        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(start);
        xAxis.setUpperBound(end);
        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(findMiny());
        yAxis.setUpperBound(result);
        chtr.setCreateSymbols(false);
        for(double x = start; x <= end; x+=0.01) {
            series.getData().add(new XYChart.Data(x, resultFunction.y(x)));
        }
        chtr.getData().add(series);
        final double lowerX = xAxis.getLowerBound();
        final double upperX = xAxis.getUpperBound();
        chtr.setOnScroll(new EventHandler<ScrollEvent>() {

            @Override
            public void handle(ScrollEvent event) {
                final double minX = xAxis.getLowerBound();
                final double maxX = xAxis.getUpperBound();
                double threshold = minX + (maxX - minX) / 2d;
                double x = event.getX();
                double value = xAxis.getValueForDisplay(x).doubleValue();
                double direction = event.getDeltaY();
                if (direction > 0) {
                    if (maxX - minX <= 1) {
                        return;
                    }
                    if (value < threshold) { // if(value > threshold)
                        xAxis.setLowerBound(minX + 1);
                    } else {
                        xAxis.setUpperBound(maxX - 1);
                    }
                } else {
                    if (value < threshold) {
                        double nextBound = Math.max(lowerX, minX - 1);
                        xAxis.setLowerBound(nextBound);
                    } else {
                        double nextBound = Math.min(upperX, maxX + 1);
                        xAxis.setUpperBound(nextBound);
                    }
                }
            }
        });


        chartPane.getChildren().add(chtr);
    }

    @FXML
    public void generateReport(){
        FileChooser chooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("HTML files (*.html)", "*.htm");
        chooser.getExtensionFilters().add(extFilter);
        chooser.setTitle("Open File");
        if (!getTxtFieldResult.getText().equals("")) {
            File file = chooser.showOpenDialog(new Stage());
            if (file != null) {
                makeReport(file);
                try {
                    java.awt.Desktop.getDesktop().open(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText("Please Select a File");
                alert.showAndWait();
            }
        }
        else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("You do not found root ");
            alert.showAndWait();
        }
    }

    public void makeReport(File file){
        ArrayList<Point> flist = new ArrayList<>();
        for(double x = start; x <= end; x++){
            flist.add(new Point(x, fFunction.y(x)));
        }
        fFunction.setPointListF(flist);

        ArrayList<Point> glist = new ArrayList<>();
        for(double x = start; x <= end; x++){
            glist.add(new Point(x, gFunction.y(x)));
        }
        gFunction.setPointListG(glist);
        try (PrintWriter out = new PrintWriter(
                new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            out.printf("<html>%n");

            out.printf("<head>%n");
            out.printf("<meta http-equiv='Content-Type' content='text/html; " +
                    "charset=UTF-8'>%n");
            out.printf("</head>%n");
            out.printf("<body bgcolor=\"#E6E6FA\">%n");
            out.printf("<h2>Звіт</h2>%n");
            out.printf("<p>Вихідні дані для функції f:</p>%n");
            out.printf("<table border = '1' cellpadding=4 ");
            for (int i = 0; i < resultFunction.xyCount(); i++) {
                out.printf("<tr>%n");
                out.printf("<td>%d</td>", i);
                out.printf("<td>%8.3f</td>%n", fFunction.getX(i));
                out.printf("<td>%8.3f</td>%n", fFunction.getY(i));
                out.printf("</tr>%n");
            }
            out.printf("</table%n");
            out.printf("<p>Вихідні дані для функції g:</p>%n");
            out.printf("<table border = '1' cellpadding=4 ");
            for (int i = 0; i < resultFunction.xyCount(); i++) {
                out.printf("<tr>%n");
                out.printf("<td>%d</td>", i);
                out.printf("<td>%8.3f</td>%n", gFunction.getX(i));
                out.printf("<td>%8.3f</td>%n", gFunction.getY(i));

                out.printf("</tr>%n");
            }
            out.printf("</table%n");
            out.printf("<p>Вихідні дані для функції f-g:</p>%n");
            out.printf("<table border = '1' cellpadding=4 ");
            for (int i = 0; i < resultFunction.xyCount(); i++) {
                out.printf("<tr>%n");
                out.printf("<td>%d</td>", i);
                out.printf("<td>%8.3f</td>%n", resultFunction.getX(i));
                out.printf("<td>%8.3f</td>%n", resultFunction.getY(i));

                out.printf("</tr>%n");
            }
            out.printf("</table%n");

            out.printf("<p>Виходячи з данних максимальне значення функції f-g = " + result + "</p>%n");
        }
        catch (IOException e){

        }
    }

    @FXML
    public void showHTU() {
        try {
            File pdfFile = new File("D://HTU.pdf");
            if (pdfFile.exists()) {
                if (Desktop.isDesktopSupported()) {
                    Desktop.getDesktop().open(pdfFile);
                } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText(null);
                    alert.setContentText("Awt Desktop is not supported!");
                    alert.showAndWait();
                }

            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Some problems with file!");
                alert.showAndWait();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
