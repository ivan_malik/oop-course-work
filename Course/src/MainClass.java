import Controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainClass extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("View/AppOverview.fxml"));
        primaryStage.setTitle("Find max value using dichotomy method ");
        primaryStage.setScene(new Scene(root, 740, 440));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
