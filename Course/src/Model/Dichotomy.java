package Model;


public class Dichotomy implements MaxValue {
    private double start, end, eps;
    private AbstractFunction function;

    public double getStart() {
        return start;
    }

    public void setStart(double start) {
        this.start = start;
    }

    public double getEnd() {
        return end;
    }

    public void setEnd(double end) {
        this.end = end;
    }

    public double getEps() {
        return eps;
    }

    public void setEps(double eps) {
        this.eps = eps;
    }

    public AbstractFunction getFunction() {
        return function;
    }

    public void setFunction(AbstractFunction function) {
        this.function = function;
    }

    public Dichotomy(double start, double end, double eps, AbstractFunction f) throws Exception {
        if(start > end){
            throw new Exception();
        }
        this.start = start;
        this.end = end;
        this.eps = eps;
        function = f;
    }

    @Override
    public double FindMax() {
        int c = -1;
        double x;
        do {
            x = (start + end) / 2;
            double f1 = function.y(x - eps);
            double f2 = function.y(x + eps);
            if ((c * f1) < (c * f2))
                end = x;
            else
                start = x;
        }
        while ((Math.abs(end-start) > eps));
        x = (start + end)/2;
        double FM = function.y(x);
        return FM;
    }
}