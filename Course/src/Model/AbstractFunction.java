package Model;

public abstract class AbstractFunction implements ExtendedFunction {
    public abstract int xyCount();
    public abstract double getX(int i);
    public abstract double getY(int i);
    public abstract void setX(int i, double x);
    public abstract void setY(int i, double y);
}
