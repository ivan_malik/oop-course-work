package Model;


import ParserFormula.MatchParser;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

public class FFunction extends AbstractFunction implements java.io.Serializable{

    private String parseToFunction;

    private ArrayList<Point> pointListF;

    public FFunction(){ }

    public FFunction(String toParse){
        parseToFunction = toParse;
    }

    public FFunction(ArrayList<Point> alp){
        pointListF = alp;
    }

    @Override
    public int xyCount() {
        return pointListF.size();
    }

    @Override
    public double getX(int i) {
        return pointListF.get(i).getX();
    }

    @Override
    public double getY(int i) {
        return pointListF.get(i).getY();
    }

    @Override
    public void setX(int i, double x) {
        pointListF.get(i).setX(x);
    }

    @Override
    public void setY(int i, double y) {
        pointListF.get(i).setY(y);
    }

    public String getParseToFunction() {
        return parseToFunction;
    }

    public void setParseToFunction(String parseToFunction) {
        this.parseToFunction = parseToFunction;
    }

    public ArrayList<Point> getPointListF() {
        return pointListF;
    }

    public void setPointListF(ArrayList<Point> pointListF) {
        this.pointListF = pointListF;
    }

    @Override
    public double applyAsDouble(double operand) {
        MatchParser mr = new MatchParser();
        mr.setVariable("x", operand);
        try {
            return mr.Parse(parseToFunction);
        } catch (Exception e) {
            throw new RuntimeException("Input Error");
        }
    }
}
