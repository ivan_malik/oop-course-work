package Model;

import java.util.function.DoubleUnaryOperator;

public interface ExtendedFunction extends DoubleUnaryOperator {
    default double y(double x) {
        return applyAsDouble(x);
    }
}