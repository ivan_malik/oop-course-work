package Model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

public class GFunction extends AbstractFunction implements java.io.Serializable {

    private ArrayList<Point> pointListG;

    public GFunction() { }

    public GFunction(ArrayList<Point> list){
        pointListG = list;
    }

    public void clearGList(){
        if(pointListG == null)
            pointListG = new ArrayList<>();
        pointListG.clear();
    }

    public ArrayList<Point> getPointListG() {
        return pointListG;
    }

    public void setPointListG(ArrayList<Point> pointListG) {
        this.pointListG = pointListG;
    }

    public void addPoint(Point p){
        pointListG.add(p);
    }

    public Point getPoint(int i){
        return pointListG.get(i);
    }

    @Override
    public int xyCount() {
        return pointListG.size();
    }

    @Override
    public double getX(int i) {
        return pointListG.get(i).getX();
    }

    @Override
    public double getY(int i) {
        return pointListG.get(i).getY();
    }

    @Override
    public void setX(int i, double x) {
        pointListG.get(i).setX(x);
    }

    @Override
    public void setY(int i, double y) {
        pointListG.get(i).setY(y);
    }

    @Override
    public double applyAsDouble(double operand) {
        double[] ax = new double[pointListG.size()];
        double[] ay = new double[pointListG.size()];
        for(int i = 0; i < pointListG.size(); i++){
            ax[i] = pointListG.get(i).getX();
            ay[i] = pointListG.get(i).getY();
        }
        double result = 0;
        for (int i = 0; i < ax.length; i++) {
            double k = 1;
            for (int j = 0; j < ay.length; j++) {
                if (j != i) {
                    k *= (operand - ax[j]) / (ax[i] - ax[j]);
                }
            }
            result += k * ay[i];
        }
        return result;
    }
}
