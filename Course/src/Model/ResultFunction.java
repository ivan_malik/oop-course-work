package Model;


import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlRootElement(name = "ResultFunction")
public class ResultFunction extends AbstractFunction implements java.io.Serializable {

    private FFunction fFunction;

    private GFunction gFunction;

    private ArrayList<Point> resultPointArrayList;

    private double start;

    private  double end;

    public ResultFunction() {

    }

    @XmlElement
    public double getEnd() {
        return end;
    }

    public void setEnd(double end) {
        this.end = end;
    }

    @XmlElement
    public FFunction getfFunction() {
        return fFunction;
    }

    public void setfFunction(FFunction fFunction) {
        this.fFunction = fFunction;
    }

    public void setResultPointArrayList(ArrayList<Point> list){
        resultPointArrayList = list;
    }

    @XmlElement(name = "resultPointArrayList")
    @XmlElementWrapper
    public ArrayList<Point> getResultPointArrayList(){
        return resultPointArrayList;
    }

    @XmlElement
    public GFunction getgFunction() {
        return gFunction;
    }

    public void setgFunction(GFunction gFunction) {
        this.gFunction = gFunction;
    }

    @XmlElement
    public double getStart() {
        return start;
    }

    public void setStart(double start) {
        this.start = start;
    }

    public ResultFunction(FFunction ff, GFunction gg){
        fFunction = ff;
        gFunction = gg;
    }

    @Override
    public double applyAsDouble(double operand) {
        return (fFunction.y(operand) - gFunction.y(operand));
    }

    public int xyCount() {
        return resultPointArrayList.size();
    }

    public void addPoint(Point p){
        resultPointArrayList.add(p);
    }

    public Point getPoint(int i){
        return resultPointArrayList.get(i);
    }

    @Override
    public double getX(int i) {
        return resultPointArrayList.get(i).getX();
    }

    @Override
    public double getY(int i) {
        return resultPointArrayList.get(i).getY();
    }

    @Override
    public void setX(int i, double x) {
        resultPointArrayList.get(i).setX(x);
    }

    @Override
    public void setY(int i, double y) {
        resultPointArrayList.get(i).setY(y);
    }
}
